#
# Hello world Waf script
#
from __future__ import print_function
import os 

rtems_version = "6"

try:
    import rtems_waf.rtems as rtems
except:
    print('error: no rtems_waf git submodule')
    import sys
    sys.exit(1)

def init(ctx):
    rtems.init(ctx, version = rtems_version, long_commands = True)

def bsp_configure(conf, arch_bsp):
    # Add BSP specific configuration checks
    pass

def options(opt):
    rtems.options(opt)

def configure(conf):
    rtems.configure(conf, bsp_configure = bsp_configure)

def build(bld):
    rtems.build(bld)

    COMMON_FLAGS = ['-DTF_LITE_STATIC_MEMORY', \
                    '-fno-unwind-tables', \
                    '-ffunction-sections', \
                    '-fdata-sections', \
                    '-fmessage-length=0', \
                    '-O0', \
                    '-mcmodel=medany', \
                    '-mabi=lp64d', \
                    '-march=rv64imafdc', \
                    '-g']

    CXXFLAGS = ['-std=c++11', \
                '-fno-rtti', \
                '-fno-exceptions', \
                '-fno-threadsafe-statics']
    
    # the riscv flags are actually provided already by rtems
    RISCV_FLAGS = ['-mcmodel=medany', \
                   '-mabi=lp64d', \
                   '-march=rv64imafdc']

    CCFLAGS = ['-std=c11']

    # waf changes two directories up so + '-I../..'
    INCLUDES = ['-I../../' + include for include in ['../../', '../../third_party/flatbuffers/include/', '../../third_party/gemmlowp/']]

    LD_FLAGS = ['-L../../../../gen', \
                '-ltflm', \
                '-lstdc++', \
                '-lm']

    SOURCES = ['init.c', \
               'apptask.c', \
               'main_functions.cc', \
               'constants.cc', \
               'model_data.cc', \
               'output_handler.cc']

    bld(features = 'c cprogram',
        target = 'tflm_sine_fp32_rtems6',
        cflags = [*COMMON_FLAGS, *CCFLAGS, *INCLUDES],
        cppflags = [*COMMON_FLAGS, *CXXFLAGS, *INCLUDES],
        source = [*SOURCES],
        ldflags = [*RISCV_FLAGS, *LD_FLAGS])
